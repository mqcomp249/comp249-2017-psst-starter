<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{{title}}</title>
    <link rel="stylesheet" href="/static/psst.css" type="text/css">
  </head>
  <body>

    <div>
    <h1>{{title}}</h1>

        <p>{{content}}</p>

        <p>Go read the <a href="/static/assignment.html">Assignment Requirements</a> and get to work.</p>
    </div>
  </body>
</html>
